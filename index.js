//Soal Nomor 1

var Hewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
//Jawaban No. 1
Hewan.sort()
console.log(Hewan)
//===============================================================================================

// Soal Nomor 2
//Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"

// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


//Jawaban No. 2
 
var data = ["Nama saya John," , "umur saya 30 tahun," , "alamat saya di Jalan Pelesiran," , "dan saya punya hobby yaitu Gaming." ]
 
var perkenalan = data.join(" ")
console.log(perkenalan)
//=============================================================================================

//Soal No. 3
// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.
//Hitung vokal untuk Muhammad
function vokal_1(Muhammad = 3){
  return Muhammad
}

console.log(vokal1())

//Hitung vokal untuk Iqbal
function vokal_2(Iqbal = 2){
    return Iqbal
}

//console.log(vokal_2())

//Hitung hasil vokal_1 dan vokal_2

var Penjumlahan = function(vokal_1, vokal_2) {   
  return vokal_1 + vokal_2  
 }
 console.log(Penjumlahan(3,2))

//=============================================================================================

//Soal Nomor 4
//Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

// console.log( hitung(0) ) // -2
function hitung(angka) {
    return angka - 2
  }
var tampung = hitung(0);
console.log(tampung)

// console.log( hitung(1) ) // 0
function hitung(angka) {
    return angka - 1
  }
var tampung = hitung(1);
console.log(tampung)

// console.log( hitung(2) ) // 2
function hitung(angka) {
    return angka * 1
  }
var tampung = hitung(2);
console.log(tampung)

// console.log( hitung(3) ) // 4
function hitung(angka) {
    return angka + 1
  }
var tampung = hitung(3);
console.log(tampung)

// console.log( hitung(5) ) // 8
function hitung(angka) {
    return angka + 3
  }
var tampung = hitung(5);
console.log(tampung)


